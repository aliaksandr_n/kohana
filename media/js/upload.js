$(function () {
    $('#multi').MultiFile({
        accept: 'jpg|gif|png', max: 15, STRING: {
            remove: '<img src="/media/img/delete.png"> ',
            selected: 'Выбраны: $file',
            denied: 'Неверный тип файла: $ext! Разрешенные расширения: .jpg, .gif, .png.',
            duplicate: 'Этот файл уже выбран:\n$file!'
        }
    });
});
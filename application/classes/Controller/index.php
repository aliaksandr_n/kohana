<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Basic main page's class
 */
class Controller_Index extends Controller_Base {

    public $template = 'index/v_base';

    public function  before() {

        parent::before();
        // Widget
        $topmenu = $this->widget_load('topmenu');
        // Template
        $this->template->styles[] = 'media/css/style.css';
        $this->template->top_menu = $topmenu;
    }
}

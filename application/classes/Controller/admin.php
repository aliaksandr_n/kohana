<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Basic main page's class
 */
class Controller_Admin extends Controller_Base {

    public $template = 'admin/v_base';

    public function  before() {

        parent::before();
        if (!$this->auth->logged_in()) {
            $this->redirect('login');
        }
        // Widget
        $menu_admin = $this->widget_load('menuadmin');
        // Template
        $this->template->styles[] = 'media/css/style.css';
        $this->template->styles[] = 'media/css/style_admin.css';
        $this->template->menuadmin = $menu_admin;
    }
}

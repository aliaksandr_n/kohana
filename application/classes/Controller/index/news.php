<?php defined('SYSPATH') or die('No direct script access.');
/*
 * News
 */
class Controller_Index_News extends Controller_Index {

    public function action_index() {

        $all_news = Model::factory('new')->all_news();
        $content = View::factory('index/news/v_news_index', array(
            'all_news' => $all_news,
            )
        );
        // Template
        $this->template->title = 'Новости';
        $this->template->page_title = 'Новости';
        $this->template->block_center = array($content);
    }

    public function action_get() {

        $id = (int)$this->request->param('id');
        $news = Model::factory('new')->get_news($id);
        
        if(!is_array($news)){
            $this->redirect('admin/news');
        }
        $content = View::factory('index/news/v_news_get', array(
                'news' => $news,
            ));
        // Template
        $this->template->title = $news['title'];
        $this->template->page_title = HTML::anchor('news', 'Новости') . " &rarr; ".  $news['title'];
        $this->template->block_center = array($content);
    }
}

<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Pictures
 */
class Controller_Index_Pictures extends Controller_Index {

    public function before() {

        parent::before();
        // Modal window with picture
        $this->template->scripts[] = 'media/js/jquery-2.1.3.js';
        $this->template->styles[] = 'media/css/modal.css';
        $this->template->scripts[] = 'media/js/modal.js';
        // /Modal window with picture
    }

    public function action_index() {

        $pictures = Model::factory('picture')->all_pictures();
        $content = View::factory('index/pictures/v_pictures_index', array(
            'pictures' => $pictures,
            )
        );
        // Template
        $this->template->title = 'Галереи изображений';
        $this->template->page_title = 'Галереи изображений';
        $this->template->block_center = array($content);
    }

    public function action_view() {

        $id = (int)$this->request->param('id');
        $picture = Model::factory('picture');
        $data = $picture->get_one_picture($id);
        if(!is_array($data)) {
            $this->redirect('admin/pictures');
        }
        $data['images'] = $picture->all_images_by_pic($id);
        $content = View::factory('index/pictures/v_pictures_view')
            ->bind('id', $id)
            ->bind('data', $data);
        // Template
        $this->template->page_title = HTML::anchor('pictures', 'Галереи изображений') . " &rarr; " . $data['title'];
        $this->template->block_center = array($content);
    }
}

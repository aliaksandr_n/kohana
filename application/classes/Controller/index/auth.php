<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Authorization
 */
class Controller_Index_Auth extends Controller_Index {

    public function action_index() {

        $this->action_login();
    }

    public function action_login() {

        if (Auth::instance()->logged_in()) {
            $this->redirect('admin/main');
        }
        if (isset($_POST['submit'])){
            $data = Arr::extract($_POST, array('username', 'password', 'remember'));
            $status = Auth::instance()->login($data['username'], $data['password'], (bool)$data['remember']);
            if ($status){
                if(Auth::instance()->logged_in()) {
                    $this->redirect('admin');
                }
                $this->redirect('admin');
            }
            else {
                $errors = array(Kohana::message('auth/user', 'no_user'));
            }
        }
        $content = View::factory('index/auth/v_auth_login')
                    ->bind('errors', $errors)
                    ->bind('data', $data);
        // Template
        $this->template->title = 'Вход';
        $this->template->page_title = 'Вход';
        $this->template->block_center = array($content);
    }

    public function action_logout() {

        if(Auth::instance()->logout()) {
            $this->redirect();
        }
    }
}

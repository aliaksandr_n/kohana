<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Pages
 */
class Controller_Index_Page extends Controller_Index {

    public function action_index() {

        $alias = $this->request->param('page_alias');
        $page = Model::factory('page')->get_page_by_alias($alias);
        if(!is_array($page)) {
            $this->redirect('');
        }
        $content = View::factory('index/page/v_page_index', array(
            'page' => $page,
        ));
        // Template
        $this->template->title = $page['title'];
        $this->template->page_title = $page['title'];
        $this->template->block_center = array($content);
    }
}

<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Widget "Menu admin"
 */
class Controller_Widgets_Menuadmin extends Controller_Template {

    public $template = 'widgets/w_menuadmin';
    
    public function action_index() {
        $select = Request::initial()->controller();
        $menu = array(
            'Главная' => array('admin/main'),
            'Страницы' => array('admin/pages'),
            'Новости' => array('admin/news'),
            'Галереи изображений' => array('admin/pictures'),
            'Выход' => array('logout'),
        );
        // Template
        $this->template->menu = $menu;
        $this->template->select = $select;
    }
}

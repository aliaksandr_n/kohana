<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Widget "Top menu"
 */
class Controller_Widgets_Topmenu extends Controller_Template {
    
    public $template = 'widgets/w_topmenu';

    public function action_index() {

        $select = Request::initial()->param('page_alias');
        if ($select == NULL) {
            $select = Request::initial()->action();
        }
        $menu = array(
            'Новости' => array('news'),
            'Галереи изображений' => array('pictures'),
        );
        // Template
        $this->template->menu = $menu;
        $this->template->select = $select;
    }
}

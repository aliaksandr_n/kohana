<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Common basic class
 */
class Controller_Base extends Controller_Template {

    protected $user;

    protected $auth;

    protected $widgets_folder = 'widgets'; //Te folder for widget's controllers

    public function before() {

        parent::before();
        $settings = Kohana::$config->load('settings');
        $this->auth = Auth::instance();
        $this->user = $this->auth->get_user();
        // Template
        $this->template->site_name = $settings->site_name;
        $this->template->site_description = $settings->site_description;
        $this->template->page_title = null;
        $this->template->title = null;
        // Include styles and scripts
        $this->template->styles = array();
        $this->template->scripts = array();
        // Include blocks
        $this->template->block_left = null;
        $this->template->block_center = null;
        $this->template->block_right = null;
    }

    public function widget_load($widget) {

        return Request::factory($this->widgets_folder . '/' . $widget)->execute();
    }
}

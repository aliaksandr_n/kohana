<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Pages
 */
class Controller_Admin_Pages extends Controller_Admin {

    public function before() {

        parent::before();
        // Template
        $this->template->page_title = 'Страницы';
    }

    public function action_index() {

        $pages = Model::factory('page')->all_pages();
        $content = View::factory('admin/pages/v_pages_index', array(
            'pages' => $pages,
        ));
        // Template
        $this->template->block_center = array($content);
    }

    public function action_add() {

        if (isset($_POST['submit'])) {
            $post_data = Arr::extract($_POST, array('title', 'alias', 'text'));
            $data = Arr::map('trim', $post_data);
            //Validation
            $valid = $this->rules_and_labels($data);
            if ($valid->check()) {
                $page_result = Model::factory('page')->add_page($data);
                $this->redirect('admin/pages');
            }
            else {
                $errors = $valid->errors('validation');
            }
        }
        $content = View::factory('admin/pages/v_pages_add')
                ->bind('errors', $errors)
                ->bind('data', $data);
        // Template
        $this->template->page_title .= ':: Добавить';
        $this->template->block_center = array($content);
    }

    public function action_edit() {

        $id = (int)$this->request->param('id');
        $page = Model::factory('page');
        $data = $page->get_page($id);
        if(!is_array($data)){
            $this->redirect('admin/pages');
        }
        // Modifying
        if (isset($_POST['submit'])) {
            $post_data = Arr::extract($_POST, array('title', 'alias', 'text'));
            $data = Arr::map('trim', $post_data);
            if ($id == '1') {
                $data['alias'] = 'index';
            }
            //Validation
            $valid = $this->rules_and_labels($data);
            if ($valid->check()) {
                $page_result = $page->update_page($data, $id);
                $this->redirect('admin/pages');
            }
            else {
                $errors = $valid->errors('validation');
            }
        }
        $content = View::factory('admin/pages/v_pages_edit')
                ->bind('id', $id)
                ->bind('errors', $errors)
                ->bind('data', $data);
        // Template
        $this->template->page_title .= ' :: Редактировать';
        $this->template->block_center = array($content);
    }

    public function action_delete() {

        $id = (int)$this->request->param('id');
        if ($id <> '1') {
            $page_result = Model::factory('page')->delete_page($id);
        }
        $this->redirect('admin/pages');
    }

    private function rules_and_labels($data) {

        $valid = Validation::factory($data);
        //Rules
        $valid->rule('title', 'not_empty')
            ->rule('alias', 'not_empty')
            ->rule('text', 'not_empty')
            // Labels
            ->label('title', 'Название')
            ->label('alias', 'Путь')
            ->label('text', 'Текст');
        return $valid;
    }
}

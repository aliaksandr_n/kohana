<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Admin_News extends Controller_Admin {

    public function before() {

        parent::before();
        // Template
        $this->template->page_title = 'Новости';
    }

    public function action_index() {

        $all_news = Model::factory('new')->all_news();
        $content = View::factory('admin/news/v_news_index')
            ->bind('all_news', $all_news);
        // Template
        $this->template->block_center = array($content);
    }

    public function action_edit() {

        $id = (int)$this->request->param('id');
        $new = Model::factory('new');
        $data = $new->get_news($id);
        if(!is_array($data)) {
            $this->redirect('admin/news');
        }
        if (isset($_POST['submit'])) {
            $post_data = Arr::extract($_POST, array('title', 'intro', 'content', 'date'));
            $input_data = Arr::map('trim', $post_data);
            //Validation
            $valid = $this->rules_and_labels($input_data);
            if ($valid->check()) {
                $new->update_news($input_data, $id);
                $this->redirect('admin/news');
            }
            else {
                $errors = $valid->errors('validation');
            }
        }
        $content = View::factory('admin/news/v_news_edit')
            ->bind('id', $id)
            ->bind('errors', $errors)
            ->bind('data', $data);
        // Template
        $this->template->page_title .= ' :: Редактировать';
        $this->template->block_center = array($content);
    }

    public function action_add() {

        if (isset($_POST['submit'])) {
            $post_data = Arr::extract($_POST, array('title', 'intro', 'content', 'date'));
            $data = Arr::map('trim', $post_data);
            //Validation
            $valid = $this->rules_and_labels($data);
            if ($valid->check()) {
                Model::factory('new')->add_news($data);
                $this->redirect('admin/news');
            }
            else {
                $errors = $valid->errors('validation');
            }
        }
        $content = View::factory('admin/news/v_news_add')
            ->bind('errors', $errors)
            ->bind('data', $data);
        // Template
        $this->template->page_title .= ':: Добавить';
        $this->template->block_center = array($content);
    }

    public function action_delete() {

        $id = (int)$this->request->param('id');
        $pages_result = Model::factory('new')->delete_news($id);
        $this->redirect('admin/news');
    }

    private function rules_and_labels($data) {

        $valid = Validation::factory($data);
        //Rules
        $valid->rule('title', 'not_empty')
            ->rule('intro', 'not_empty')
            ->rule('content', 'not_empty')
            ->rule('date', 'not_empty')
            ->rule('date', 'date')
            // Labels
            ->label('title', 'Название')
            ->label('date', 'Дата')
            ->label('intro', 'Вступительный текст')
            ->label('content', 'Основной текст');
        return $valid;
    }
}

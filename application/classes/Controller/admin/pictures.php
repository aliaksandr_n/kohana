<?php defined('SYSPATH') or die('No direct script access.');
/*
 * Pictures
 */
class Controller_Admin_Pictures extends Controller_Admin {

    public function before() {

        parent::before();
        $this->template->scripts[] = 'media/js/jquery-2.1.3.js';
        $this->template->scripts[] = 'media/js/jquery.MultiFile.pack.js';
        $this->template->scripts[] = 'media/js/upload.js';
        // Modal window with picture
        $this->template->styles[] = 'media/css/modal.css';
        $this->template->scripts[] = 'media/js/modal.js';
        // /Modal window with picture
    }

    public function action_index() {

        $pictures = Model::factory('picture')->all_pictures();
        $content = View::factory('admin/pictures/v_pictures_index', array(
            'pictures' => $pictures,
        ));
        // Template
        $this->template->page_title = 'Галереи изображений';
        $this->template->block_center = array($content);
    }

    public function action_add() {

        if (isset($_POST['submit'])) {
            // Action with pictures
            $post_data = Arr::extract($_POST, array('title'));
            $data = Arr::map('trim', $post_data);
            //Validation
            $valid = $this->rules_and_labels($data);
            if ($valid->check()) {
                $picture = Model::factory('picture');
                $picture_id = $picture->add_picture($data);
                // Action with images
                if (!empty($_FILES['images']['name'][0])) {
                    foreach ($_FILES['images']['tmp_name'] as $image) {
                        $filename = $this->_upload_img($image);
                        // Insert into db
                        $picture->add_image($picture_id, $filename);
                    }
                }
                $this->redirect('admin/pictures/edit/' . $picture_id);
            }
            else {
                $errors = $valid->errors('validation');
            }
        }
        $content = View::factory('admin/pictures/v_pictures_add')
                ->bind('errors', $errors)
                ->bind('data', $data);
        // Template
        $this->template->page_title = 'Галереи изображений :: Добавить';
        $this->template->block_center = array($content);
    }

    public function action_edit() {

        $id = (int)$this->request->param('id');
        $picture = Model::factory('picture');
        $data = $picture->get_one_picture($id);
        $data['images'] = $picture->all_images_by_pic($id);
        // Editing
        if (isset($_POST['submit'])) {
            // Action with pictures
            $post_data = Arr::extract($_POST, array('title'));
            $data = Arr::map('trim', $post_data);
            //Validation
            $valid = $this->rules_and_labels($data);
            if ($valid->check()) {
                $picture->update_picture($id, $data);
                // Action with images
                if (!empty($_FILES['images']['name'][0])){
                    foreach ($_FILES['images']['tmp_name'] as $image){
                        $filename = $this->_upload_img($image);
                        // Insert into db
                        $picture->add_image($id, $filename);
                    }
                }
                $this->redirect('admin/pictures/edit/' . $id);
            }
            else {
                $errors = $valid->errors('validation');
            }
        }
        $content = View::factory('admin/pictures/v_pictures_edit')
                ->bind('id', $id)
                ->bind('errors', $errors)
                ->bind('data', $data);
        // Вывод в шаблон
        $this->template->page_title = 'Галереи изображений :: Редактировать';
        $this->template->block_center = array($content);
    }

    public function action_delete() {

        $id = (int)$this->request->param('id');
        if(!$id) {
            $this->redirect('admin/pictures');
        }
        Model::factory('picture')->delete_picture($id);
        $this->redirect('admin/pictures');
    }

    public function _upload_img($file, $ext = NULL, $directory = NULL){

        if($directory == NULL) {
            $directory = 'media/uploads';
        }
        if($ext== NULL) {
            $ext= 'jpg';
        }
        // Generate random name
        $symbols = '0123456789abcdefghijklmnopqrstuvwxyz';
        $filename = '';
        for($i = 0; $i < 10; $i++) {
            $filename .= rand(1, strlen($symbols));
        }
        // Change size and upload image
        $im = Image::factory($file);
        if($im->width > 150){
            $im->resize(150);
        }
        $im->save("$directory/small_$filename.$ext");
        $im = Image::factory($file);
        $im->save("$directory/$filename.$ext");
        return "$filename.$ext";
    }

    public function action_delete_img() {

        $directory = 'media/uploads';
        $id = (int)$this->request->param('id');
        if(!$id) {
            $this->redirect('admin/pictures');
        }
        $picture = Model::factory('picture');
        $image = $picture->get_one_img($id);
        $picture->delete_img($image['name']);
        $small_img = $directory.'/small_'.$image['name'];
        $img = $directory.'/'.$image['name'];
        if(file_exists($small_img)) {
            unlink($small_img);
        }
        if(file_exists($img)) {
            unlink($img);
        }
        $this->redirect('admin/pictures/edit/'.$image['picture_id']);
    }

    private function rules_and_labels($data) {

        $valid = Validation::factory($data);
        //Rules
        $valid->rule('title', 'not_empty')
            // Labels
            ->label('title', 'Название');
        return $valid;
    }
}

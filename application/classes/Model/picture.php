<?php defined('SYSPATH') or die('No direct script access.');

class Model_Picture extends Model {

    public function all_pictures($limit = NULL) {

        $limit = (int)$limit;
        if($limit == NULL) {
            $query = DB::select()->from('pictures')->order_by('id', 'DESC');
            return $query->execute();
        }
        else {
            $query = DB::select()->from('pictures')->order_by('id', 'DESC')->limit($limit);
            return $query->execute();
        }
    }

    public function get_one_picture($id) {

        $id = (int)$id;
        $query = DB::select()->from('pictures')->where('id', '=', $id);
        $result = $query->execute();
        return $result[0];
    }

    public function all_images_by_pic($id) {

        $id = (int)$id;
        $query = DB::select()->from('pictures')->join('images')->on('pictures.id', '=', 'images.picture_id')->where('pictures.id', '=', $id);
        $result = $query->execute();
        return $result;
    }

    public function add_picture($data){

        $title = $data['title'];
        $query = DB::insert('pictures', array('title'))->values(array($title));
        $result = $query->execute();
        return $result[0];
    }

    public function add_image($picture_id, $filename){

        $query = DB::insert('images', array('picture_id', 'name'))->values(array($picture_id, $filename));
        $result = $query->execute();
        return $result[0];
    }

    public function update_picture($id, $data) {

        $title = $data['title'];
        $query = DB::update('pictures')->set(array(
                'title' => $title,
            ))
            ->where('id', '=', $id);
        return $query->execute();
    }

    public function delete_picture($id) {

        $query = DB::delete('pictures')->where('id', '=', $id);
        return $query->execute();
    }

    public function get_one_img($id) {

        $id = (int)$id;
        $query = DB::select()->from('images')->where('id', '=', $id);
        $result = $query->execute();
        return $result[0];
    }

    public function delete_img($name) {

        $query = DB::delete('images')->where('name', '=', $name);
        return $query->execute();
    }
} 

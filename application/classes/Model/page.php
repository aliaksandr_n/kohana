<?php defined('SYSPATH') or die('No direct script access.');

class Model_Page extends Model {

    public function all_pages($limit = NULL) {

        $limit = (int)$limit;
        if($limit == NULL) {
            $query = DB::select()->from('pages')->order_by('id', 'ASC');
            return $query->execute();
        }
        else {
            $query = DB::select()->from('pages')->order_by('id', 'ASC')->limit($limit);
            return $query->execute();
        }
    }

    public function get_page($id) {

        $id = (int)$id;
        $query = DB::select()->from('pages')->where('id', '=', $id);
        $result = $query->execute();
        return $result[0];
    }

    public function get_page_by_alias($alias) {

        $query = DB::select()->from('pages')->where('alias', '=', $alias);
        $result = $query->execute();
        return $result[0];
    }

    public function add_page($data) {

        $title = $data['title'];
        $alias = $data['alias'];
        $text = $data['text'];
        $query = DB::insert('pages', array('title', 'alias', 'text'))->values(array($title, $alias, $text));
        return $query->execute();
    }

    public function update_page($data, $id) {

        $title = $data['title'];
        $alias = $data['alias'];
        $text = $data['text'];
        $query = DB::update('pages')
            ->set(array(
                'title' => $title,
                'alias' => $alias,
                'text' => $text,
            ))
            ->where('id', '=', $id);
        return $query->execute();
    }

    public function delete_page($id) {

        $query = DB::delete('pages')->where('id', '=', $id);
        return $query->execute();
    }
} 

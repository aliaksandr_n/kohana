<?php defined('SYSPATH') or die('No direct script access.');

class Model_New extends Model {

    public function all_news($limit = NULL) {

        $limit = (int)$limit;
        if($limit == NULL) {
            $query = DB::select()->from('news')->order_by('id', 'DESC');
            return $query->execute();
        }
        else {
            $query = DB::select()->from('news')->order_by('id', 'DESC')->limit($limit);
            return $query->execute();
        }
    }

    public function get_news($id) {

        $id = (int)$id;
        $query = DB::select()->from('news')->where('id', '=', $id);
        $result = $query->execute();
        return $result[0];
    }

    public function add_news($data){

        $title = $data['title'];
        $intro = $data['intro'];
        $content = $data['content'];
        if ($data['date'] == null) {
            $date = date('d.m.Y');
        }
        else {
            $date = $data['date'];
        }
        $query = DB::insert('news', array('title', 'intro', 'content', 'date'))->values(array($title, $intro, $content, $date));
        return $query->execute();
    }

    public function update_news($data, $id) {

        $title = $data['title'];
        $intro = $data['intro'];
        $content = $data['content'];
        if ($data['date'] == null) {
            $date = date('d.m.Y');
        }
        else {
            $date = $data['date'];
        }
        $query = DB::update('news')
            ->set(array(
                'title' => $title,
                'intro' => $intro,
                'content' => $content,
                'date' => $date,
            ))
            ->where('id', '=', $id);
        return $query->execute();
    }

    public function delete_news($id) {

        $query = DB::delete('news')->where('id', '=', $id);
        return $query->execute();
    }
} 

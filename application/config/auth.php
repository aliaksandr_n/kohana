<?php defined('SYSPATH') or die('No direct access allowed.');
// Auth settings
return array(
	'driver'       => 'file',
	'hash_method'  => 'sha256',
	'hash_key'     => 'asjdhas6f2e12kas',
	'lifetime'     => 1209600,
	'session_key'  => 'auth_user',
	// Username/password combinations for the Auth File driver
	'users' => array(
        'admin' => '3078cc3bbbb1e4811dc3853602f979cc2c23358b2666c173e06a141d3cec0ff6',
	),
);

<!DOCTYPE HTML>
<html>
<head>
    <title><?php echo $site_name;?> | <?php echo $page_title;?></title>
    <meta content="text/html; charset=utf8" http-equiv="content-type">
    <?php foreach ($styles as $file_style):?>
        <?php echo html::style($file_style);?>
    <?php endforeach;?>
    <?php foreach ($scripts as $file_script):?>
        <?php echo html::script($file_script);?>
    <?php endforeach;?>
</head>
    <body>
    <center>
        <table align="center" border="0" width="990" cellpadding="10" class="maintbl">
            <thead>
                <tr>
                    <th align="left" colspan="3">
                        <div id="header">
                            <br/>
                            <a href="/"><h1><?php echo $site_name;?></h1></a>
                            <h3><?php echo $site_description;?></h3>
                        <div class="top_menu">
                            <?php echo $top_menu;?>
                        </div>
                        </div>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr valign="top">
                    <!-- Left block-->
                    <?php if(isset($block_left)):?>
                        <td width="230">
                            <?php foreach ($block_left as $lblock):?>
                                <?php echo $lblock;?>
                            <?php endforeach;?>
                        </td>
                    <?php endif;?>
                    <!-- /Left block-->
                    <!-- Central block-->
                    <?php if(isset($block_center)):?>
                        <td>
                            <h2><?php echo $page_title;?></h2>
                            <?php foreach ($block_center as $cblock):?>
                                <?php echo $cblock;?>
                            <?php endforeach;?>
                        </td>
                    <?php endif;?>
                    <!-- /Central block-->
                    <!-- Right block-->
                     <?php if(isset($block_right)):?>
                        <td width="200">
                            <?php foreach ($block_right as $rblock):?>
                                <?php echo $rblock;?>
                            <?php endforeach;?>
                        </td>
                    <?php endif;?>
                    <!-- /Right block-->
                </tr>
                <tr align="center">
                    <td colspan="3" id="footer">Copyright</td>
                </tr>
            </tbody>
        </table>
    </center>
    </body>
</html>
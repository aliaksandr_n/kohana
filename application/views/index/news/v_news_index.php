<br/>
<?foreach ($all_news as $news):?>
    <h3>
        <?php echo HTML::anchor('news/get/' . $news['id'], $news['title']);?>
    </h3>
    <div class="date">
        <?php echo $news['date'];?>
    </div>
    <p>
        <?php echo $news['intro'];?>
    </p>
    <div class="block_bottom">
        <?php echo HTML::anchor('news/get/' . $news['id'], 'Читать полностью...');?>
    </div>
<?endforeach;?>

<br/>
<?php if (!empty($data['images'])):?>
    <table width="100%" cellspacing="20">
        <tr>
            <?php foreach($data['images'] as $i =>  $image):?>
                <td align="center">
                    <?php //echo html::anchor('media/uploads/'. $image['name'], html::image('media/uploads/small_' . $image['name']), array('target' => '_blank'));?>
                    <!-- Modal window with picture -->
                    <?php echo html::anchor('#modal' . $image['id'], html::image('media/uploads/small_' . $image['name']), array('target' => '_blank', 'class' => 'open_modal'));?>
                    <!-- /Modal window with picture -->
                </td>
                <?php if($i % 2):?>
                    </tr><tr>
                <?php endif;?>
                <!-- Modal window with picture -->
                <div id="overlay"></div>
                <div id="modal<?php echo $image['id'];?>" class="modal_div">
                    <span class="modal_close">X</span>
                    <?php echo html::image('media/uploads/' . $image['name']);?>
                </div>
                <!-- /Modal window with picture -->
            <?php endforeach;?>
        </tr>
    </table>
<?php else:?>
    <div class="empty">Нет изображений</div>
<?php endif;?>

<?php if($pictures->count() > 0):?>
    <br/>
    <table border="0" width="100%"  cellpadding="0" cellspacing="10">
        <tr>
            <?php foreach($pictures as $i => $picture):?>
                <?php if($i % 5 == 0):?>
                    </tr>
                    <tr>
                <?php endif;?>
                <td align="center" width="20%">
                    <?php echo html::anchor("pictures/view/".$picture['id'],
                        html::image('media/img/book.png', array('width' => '55')));?><br/>
                    <?php echo html::anchor("pictures/view/".$picture['id'], "<h4>".$picture['title']."</h4><br/>");?>
                </td>
            <?php endforeach;?>
        </tr>
    </table>
<?php else:?>
    <div class="empty">Нет изображений в этой галерее</div>
<?php endif;?>

<br/>
<table width="100%" border="0" class="tbl"  cellspacing="0">
    <thead>
        <tr height="30">
            <th>Название</th>
            <th>Функции</th>
        </tr>
    </thead>
<?php foreach ($pictures as $picture):?>
    <tr>
        <td ><?php echo HTML::anchor('admin/pictures/edit/'. $picture['id'], $picture['title']);?></td>
        <td width="100" align="center">
        <?php echo HTML::anchor('pictures/view/'. $picture['id'], HTML::image('media/img/see.png'), array('target' => '_blank'));?>
        <?php echo HTML::anchor('admin/pictures/edit/'. $picture['id'], HTML::image('media/img/edit.png'));?>
        <?php echo HTML::anchor('admin/pictures/delete/'. $picture['id'], HTML::image('media/img/delete.png'));?>
        </td>
    </tr>
<?php endforeach;?>
</table>
<br/>
<p align="right">
    <?php echo HTML::image('media/img/add.png', array('valign' => 'top'));?>
    <?php echo HTML::anchor('admin/pictures/add', 'Добавить');?>
</p>

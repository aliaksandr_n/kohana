<br/>
<?php if($errors):?>
    <?php foreach ($errors as $error):?>
        <div class="error">
            <?php echo $error;?>
        </div>
    <?php endforeach;?>
<?php endif;?>
<?php echo Form::open('admin/pictures/add', array('enctype' => 'multipart/form-data'));?>
<table width="100%" cellspacing="5" >
    <tr>
        <td ><?php echo Form::label('title', 'Название');?>: <br/><?php echo Form::input('title', $data['title'], array('size' => 80));?></td>
    </tr>
    <td>
        <br/>
        <?php echo Form::label('images', 'Загрузить изображения');?>:<br/><br/>
        <?php echo Form::file('images[]', array('id' => 'multi'));?>
    </td>
    <tr>
        <td align="center"><?php echo Form::submit('submit', 'Добавить');?></td>
    </tr>
</table>
<?php echo Form::close();?>

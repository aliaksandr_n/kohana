<br/>
<?php if($errors):?>
    <?php foreach ($errors as $error):?>
        <div class="error">
            <?php echo $error;?>
        </div>
    <?php endforeach;?>
<?php endif;?>
<?php echo Form::open('admin/pictures/edit/' . $id,  array('enctype' => 'multipart/form-data'));?>
    <table width="100%" cellspacing="5">
        <tr>
            <td ><?php echo Form::label('title', 'Название');?>: <br/><?php echo Form::input('title', $data['title'], array('size' => 80));?></td>
        </tr>
        <tr>
            <td>
                <br/>
                <?php echo Form::label('images', 'Загрузить изображения');?>: <br/><br/>
                <?php echo Form::file('images[]', array('id' => 'multi'));?>
            </td>
        </tr>
        <tr>
            <td align="center"><?php echo Form::submit('submit', 'Сохранить');?></td>
        </tr>
        <tr>
            <td>
                <?php echo Form::label('images', 'Изображения');?>: <br/><br/>
                <?php if(!empty($data['images'])):?>
                    <table width="100%" cellspacing="20">
                        <tr>
                        <?php foreach($data['images'] as $i => $image):?>
                            <td align="center">
                                <?php //echo html::anchor('media/uploads/'. $image['name'], html::image('media/uploads/small_' . $image['name']), array('target' => '_blank'));?>
                                <!-- Modal window with picture -->
                                <?php echo html::anchor('#modal' . $image['id'], html::image('media/uploads/small_' . $image['name']), array('target' => '_blank', 'class' => 'open_modal'));?>
                                <!-- /Modal window with picture -->
                                <br><?php echo html::anchor('admin/pictures/delete_img/' . $image['id'], 'Удалить');?>
                            </td>
                            <?php if($i % 2):?>
                                </tr><tr>
                            <?php endif;?>
                            <!-- Modal window with picture -->
                            <div id="overlay"></div>
                            <div id="modal<?php echo $image['id'];?>" class="modal_div">
                                <span class="modal_close">X</span>
                                <?php echo html::image('media/uploads/' . $image['name']);?>
                            </div>
                            <!-- /Modal window with picture -->
                            <?php endforeach;?>
                        </tr>
                    </table>
                <?php else:?>
                    <div class="empty">Нет изображений</div>
                <?php endif;?>
            </td>
        </tr>
    </table>
<?php echo Form::close();?>

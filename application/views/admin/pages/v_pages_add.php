<br/>
<?php if($errors):?>
    <?php foreach ($errors as $error):?>
        <div class="error">
            <?php echo $error;?>
        </div>
    <?php endforeach;?>
<?php endif;?>
<?php echo Form::open('admin/pages/add');?>
<table width="100%" cellspacing="5">
     <tr>
        <td ><?php echo Form::label('alias', 'Путь');?>:</td>
        <td><?php echo URL::base('http');?>page/ <?php echo Form::input('alias', $data['alias'], array('size' => 20));?></td>
    </tr>
    <tr>
        <td ><?php echo Form::label('title', 'Название');?>:</td>
        <td><?php echo Form::input('title', $data['title'], array('size' => 100));?></td>
    </tr>
    <tr>
        <td valign="top"><?php echo Form::label('text', 'Текст');?>: </td>
        <td><?php echo Form::textarea('text', $data['text'], array('cols' => 100, 'rows' => 20));?></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><?php echo Form::submit('submit', 'Добавить');?></td>
    </tr>
</table>
<?php echo Form::close();?>

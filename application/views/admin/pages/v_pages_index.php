<?php if($pages->count() > 0):?>
    <br/>
    <table width="100%" border="0" class="tbl"  cellspacing="0">
        <thead>
            <tr height="30">
                <th>Путь</th>
                <th>Название</th>
                <th>Функции</th>
            </tr>
        </thead>
        <?php foreach ($pages as $page):?>
            <tr>
                <td width="200" align="center" >
                    <?php echo $page['alias'];?>
                </td>
                <td >
                    <?php echo HTML::anchor('admin/pages/edit/'. $page['id'], $page['title']);?>
                </td>
                <td width="100" align="center">
                    <?php echo HTML::anchor('page/'. $page['alias'], HTML::image('media/img/see.png'), array('target' => '_blank'));?>
                    <?php echo HTML::anchor('admin/pages/edit/'. $page['id'], HTML::image('media/img/edit.png'));?>
                    <?php if($page['id'] <> '1'):?>
                        <?php echo HTML::anchor('admin/pages/delete/'. $page['id'], HTML::image('media/img/delete.png'));?>
                    <?php else:?>
                        <?php echo HTML::image('media/img/delete_blocked.png');?>
                    <?php endif;?>
                </td>
            </tr>
        <?php endforeach;?>
    </table>
<?php else:?>
    <p align="center">Нет страниц</p>
<?php endif;?>
<br/>
<p align="right">
    <?php echo HTML::image('media/img/add.png', array('valign' => 'top'));?>
    <?php echo HTML::anchor('admin/pages/add', 'Добавить');?>
</p>

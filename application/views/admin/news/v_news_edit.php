<br/>
<?php if($errors):?>
    <?php foreach ($errors as $error):?>
        <div class="error">
            <?php echo $error;?>
        </div>
    <?php endforeach;?>
<?php endif;?>
<br/>
<?php echo Form::open('admin/news/edit/'. $id);?>
<table width="100%" cellspacing="3">
    <tr>
        <td ><?php echo Form::label('title', 'Название');?>:</td>
        <td><?php echo Form::input('title', $data['title'], array('size' => 100));?></td>
    </tr>
    <tr>
        <td ><?php echo Form::label('date', 'Дата');?>:</td>
        <td><?php echo Form::input('date', $data['date'], array('size' => 100));?></td>
    </tr>
    <tr>
        <td valign="top"><?php echo Form::label('intro', 'Вступительный текст');?>:</td>
        <td><?php echo Form::textarea('intro', $data['intro'], array('cols' => 100, 'rows' => 10));?></td>
    </tr>
    <tr>
        <td valign="top"><?php echo Form::label('content', 'Основной текст');?>: </td>
        <td><?php echo Form::textarea('content', $data['content'], array('cols' => 100, 'rows' => 20));?></td>
    </tr>
    <tr>
        <td colspan="2" align="center"><?php echo Form::submit('submit', 'Сохранить');?></td>
    </tr>
</table>
<?php echo Form::close();?>

   
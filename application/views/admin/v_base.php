<!DOCTYPE HTML>
<html>
    <head>
        <title><?php echo $site_name;?> | <?php echo $page_title;?></title>
        <meta content="text/html; charset=utf8" http-equiv="content-type">
        <?php foreach ($styles as $file_style):?>
            <?php echo html::style($file_style);?>
        <?php endforeach;?>
        <?php foreach ($scripts as $file_script):?>
            <?php echo html::script($file_script);?>
        <?php endforeach;?>
    </head>
    <body>
    <center>
        <div id="main_content">
            <!-- Central block-->
            <?php if (isset($block_center)):?>
                <td>
                    <h2><?php echo $page_title;?></h2>
                    <div class="top_menu"><?php echo $menuadmin;?></div>
                    <?php foreach ($block_center as $cblock):?>
                        <?php echo $cblock;?>
                    <?php endforeach;?>
                </td>
            <?php endif;?>
            <!-- /Central block-->
        </div>
    </center>
    </body>
</html>
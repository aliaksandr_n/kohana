-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.5.38-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win32
-- HeidiSQL Версия:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных kohana
CREATE DATABASE IF NOT EXISTS `kohana` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `kohana`;


-- Дамп структуры для таблица kohana.images
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picture_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `FK_images_pictures` (`picture_id`),
  CONSTRAINT `FK_images_pictures` FOREIGN KEY (`picture_id`) REFERENCES `pictures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.images: ~20 rows (приблизительно)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
REPLACE INTO `images` (`id`, `picture_id`, `name`) VALUES
	(31, 10, '3630303013322235172.jpg'),
	(32, 10, '142871029333313223.jpg'),
	(33, 10, '15109232173662523.jpg'),
	(34, 8, '27342461952943619.jpg'),
	(35, 5, '3316211026311217227.jpg'),
	(36, 5, '282727933283114723.jpg'),
	(37, 4, '17317111816286911.jpg'),
	(38, 4, '281420153023278365.jpg'),
	(39, 8, '716223215292223299.jpg'),
	(40, 8, '2427332325152212237.jpg'),
	(41, 10, '273431201425718511.jpg'),
	(42, 10, '1332823107331564.jpg'),
	(43, 3, '61826272013141315.jpg'),
	(44, 2, '139281919361913334.jpg'),
	(45, 2, '261027131032276248.jpg'),
	(46, 1, '51231332471211722.jpg'),
	(47, 1, '381334191818362419.jpg'),
	(48, 1, '22534143471323620.jpg'),
	(49, 1, '1314203633929331811.jpg'),
	(50, 2, '45468366321213.jpg');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;


-- Дамп структуры для таблица kohana.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `intro` text,
  `content` text,
  `date` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.news: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
REPLACE INTO `news` (`id`, `title`, `intro`, `content`, `date`) VALUES
	(1, 'Новость 1', 'Краткое описание новости 1 Краткое описание новости 1 Краткое описание новости 1 Краткое описание но', 'Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1 Содержание новости 1', '16.03.2015'),
	(2, 'Новость 2', 'Краткое описание новости 2 Краткое описание новости 2 Краткое описание новости 2 Краткое описание но', 'Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2 Содержание новости 2', '16.03.2015');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;


-- Дамп структуры для таблица kohana.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.pages: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
REPLACE INTO `pages` (`id`, `title`, `alias`, `text`) VALUES
	(1, 'Главная страница', 'index', 'Сайт с новостями и изображениями'),
	(3, 'Еще страница (нет в меню- демо пример)', 'new_page', 'Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы Текст страницы'),
	(4, 'Контакты (нет в меню- демо пример)', 'contacts', 'Страница с контактами Страница с контактами Страница с контактами Страница с контактами Страница с контактами Страница с контактами Страница с контактами Страница с контактами Страница с контактами Страница с контактами Страница с контактами');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;


-- Дамп структуры для таблица kohana.pictures
CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.pictures: ~7 rows (приблизительно)
/*!40000 ALTER TABLE `pictures` DISABLE KEYS */;
REPLACE INTO `pictures` (`id`, `title`) VALUES
	(1, 'Природа'),
	(2, 'Горы'),
	(3, 'Города'),
	(4, 'Космос'),
	(5, 'Природа'),
	(8, 'Машины'),
	(10, 'Животные');
/*!40000 ALTER TABLE `pictures` ENABLE KEYS */;


-- Дамп структуры для таблица kohana.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.roles: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
REPLACE INTO `roles` (`id`, `name`, `description`) VALUES
	(1, 'login', 'Login privileges, granted after account confirmation'),
	(2, 'admin', 'Administrative user, has access to everything.');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Дамп структуры для таблица kohana.roles_users
CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `fk_role_id` (`role_id`),
  CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.roles_users: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
REPLACE INTO `roles_users` (`user_id`, `role_id`) VALUES
	(5, 1),
	(6, 1);
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;


-- Дамп структуры для таблица kohana.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(254) NOT NULL,
  `username` varchar(32) NOT NULL DEFAULT '',
  `first_name` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL,
  `logins` int(10) unsigned NOT NULL DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `username_available` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_username` (`username`),
  UNIQUE KEY `uniq_email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.users: ~2 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
REPLACE INTO `users` (`id`, `email`, `username`, `first_name`, `password`, `logins`, `last_login`, `username_available`) VALUES
	(5, 'admin@example.test', 'admin', 'Администратор', '3078cc3bbbb1e4811dc3853602f979cc2c23358b2666c173e06a141d3cec0ff6', 9, 1426599538, 0),
	(6, 'admin2@example.test', 'admin2', 'Администратор 2', '3078cc3bbbb1e4811dc3853602f979cc2c23358b2666c173e06a141d3cec0ff6', 2, 1426512553, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;


-- Дамп структуры для таблица kohana.user_tokens
CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(40) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_token` (`token`),
  KEY `fk_user_id` (`user_id`),
  KEY `expires` (`expires`),
  CONSTRAINT `user_tokens_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы kohana.user_tokens: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `user_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_tokens` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
